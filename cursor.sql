/*Declaraion de un cursor, es una variable m�s
va a apuntar a una lista, una cantidad de registros */
DECLARE
    CURSOR cur_depts IS
    SELECT
        department_id,
        department_name
    FROM
        departments;

    v_department_id    departments.department_id%TYPE;
    v_department_name  departments.department_name%TYPE;
BEGIN
    OPEN cur_depts;
    LOOP
        FETCH cur_depts INTO
            v_department_id,
            v_department_name;
        EXIT WHEN cur_depts%notfound;
        dbms_output.put_line(v_department_id
                             || ' '
                             || v_department_name);
    END LOOP;

    CLOSE cur_depts;
END;