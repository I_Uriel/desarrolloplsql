DECLARE
    CURSOR cur_countries (p_region_id NUMBER, p_country_id CHAR) IS
    SELECT  country_id, country_name
    FROM countries
    WHERE region_id = p_region_id OR country_id = 'BR';
  
BEGIN
    FOR v_country_record IN cur_countries(145,'BR')LOOP
        DBMS_OUTPUT.PUT_LINE(v_country_record.country_id||'----'||v_country_record. country_name);
    END LOOP;
END;
