CREATE OR REPLACE PACKAGE paquete_empleado IS
    PROCEDURE get_sal (
        p_id IN NUMBER,
        p_cantidad IN NUMBER
    );

END;

CREATE OR REPLACE PACKAGE bono_aguinaldo IS

    FUNCTION get_sal(
        p_id        IN employees.employee_id%TYPE,
        p_cantidad  NUMBER
    ) RETURN BOOLEAN;
 END bono_aguinaldo;
 
 CREATE OR REPLACE PACKAGE BODY bono_aguinaldo IS

    FUNCTION get_sal

     (
        p_id        IN employees.employee_id%TYPE,
        p_cantidad  NUMBER
    ) RETURN BOOLEAN IS
    BEGIN
        IF extract(MONTH FROM sysdate) = 12 THEN
            UPDATE employees
            SET
                salary = salary + p_cantidad
            WHERE
                employee_id = p_id;

            RETURN true;
        ELSE
            RETURN false;
        END IF;
    END get_sal;

END;

DECLARE
    v_confirm   BOOLEAN;
    v_id        NUMBER;
    v_cantidad  NUMBER;
BEGIN
    v_id := 120;
    v_cantidad := 2000;
    v_confirm := bono_aguinaldo.get_sal(v_id, v_cantidad);
END;