/*Devolver la suma de los salarios de todos los empleados en
El departamento especificado. El Begin es una palabra clave que permite 
indicar en el editor de m�todos el inicio de una secuencia de comandos SQL
que debe ser interpretada por la fuente de datos actual del proceso*/

DECLARE
    v_sum_sal  NUMBER(10, 2);
    v_deptno   NUMBER NOT NULL := 60;
BEGIN
    SELECT
        SUM(salary) -- group function
    INTO v_sum_sal
    FROM
        employees
    WHERE
        department_id = v_deptno;

    dbms_output.put_line('Dep #60 Salary Total: ' || v_sum_sal);
END;