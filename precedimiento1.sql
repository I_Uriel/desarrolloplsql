Procedimiento --compilado

CREATE OR REPLACE PROCEDURE add_dept IS
v_dept_id dept.department_id%TYPE;         ---cambiar dept a departments
v_dept_name dept.department_name%TYPE;   ----
BEGIN
v_dept_id := 280;
v_dept_name := 'ST-Curriculum';
INSERT INTO dept(department_id, department_name)  ----
VALUES(v_dept_id, v_dept_name);
DBMS_OUTPUT.PUT_LINE('Inserted '|| SQL%ROWCOUNT || ' row.');
END;

-- jecutar procedimiento
BEGIN
add_dept;
END;

---checar que se inserto
SELECT department_id, department_name FROM departments WHERE department_id=280;
