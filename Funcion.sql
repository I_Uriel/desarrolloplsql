/*Creacion de una funci�n*/
CREATE OR REPLACE FUNCTION tomorrow (
    p_today IN DATE
) RETURN DATE IS
    v_tomorrow DATE;
BEGIN
    SELECT
        p_today + 1  /* argumento que la funci�n realizara*/
    INTO v_tomorrow
    FROM
        dual;

    RETURN v_tomorrow;
END;

/*Una forma de ejecutar una funcion es la siguiente*/
SELECT TOMORROW(SYSDATE) AS "Tomorrow's Date"
FROM DUAL;

/*Otra forma */
BEGIN
DBMS_OUTPUT.PUT_LINE(TOMORROW(SYSDATE));
END;