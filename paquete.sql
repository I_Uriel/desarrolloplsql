/* Declaración del paquete */
CREATE OR REPLACE PACKAGE paquete_empleado IS
    PROCEDURE buscar_empleado (
        p_parametro IN VARCHAR2
    );

END;

/* Definiendo el body */
CREATE OR REPLACE PACKAGE BODY paquete_empleado IS

    PROCEDURE buscar_empleado (
        p_parametro IN VARCHAR2
    ) IS

        v_first_name  VARCHAR2(20);
        v_last_name   VARCHAR2(20);
        v_date        DATE;
        CURSOR empleado_cur IS
        SELECT
            first_name,
            last_name,
            hire_date
        FROM
            employees
        WHERE
            first_name = p_parametro;
BEGIN
        OPEN empleado_cur;
        LOOP
            FETCH empleado_cur INTO
                v_first_name,
                v_last_name,
                v_date;
            EXIT WHEN empleado_cur%notfound;
            dbms_output.put_line(' Empleado Nombre: '
                                 || v_first_name
                                 || ' Apellido: '
                                 || v_last_name
                                 || ' Fecha '
                                 || v_date)
END LOOP;

    END buscar_empleado;

END paquete_empleado;

/* Invocar paquete */
BEGIN
    paquete_empleado.buscar_empleado('Adam');
END;