/*Conversion implicita*/
DECLARE
    v_salary        NUMBER(6) := 6000;
    v_sal_increase  VARCHAR2(5) := '1000';
    v_total_salary  v_salary%TYPE;
BEGIN
    v_total_salary := v_salary + v_sal_increase;
    dbms_output.put_line(v_total_salary);
END;