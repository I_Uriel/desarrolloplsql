/*Bucles anidados */
BEGIN
    FOR v_outerloop IN 1..3 LOOP
        FOR v_innerloop IN REVERSE 1..5 LOOP
            dbms_output.put_line('Outer loop is: '
                                 || v_outerloop
                                 || ' and inner loop is: '
                                 || v_innerloop);
        END LOOP;
    END LOOP;
END;