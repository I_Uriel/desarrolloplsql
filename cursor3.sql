/*Cursor reducido, devuelve a los epleados del depto 50*/
DECLARE
    CURSOR cur_emps IS
    SELECT
        employee_id,
        last_name
    FROM
        employees
    WHERE
        department_id = 50;

BEGIN
    FOR v_emp_record IN cur_emps LOOP
        dbms_output.put_line(v_emp_record.employee_id
                             || ' '
                             || v_emp_record.last_name);
    END LOOP;
END;